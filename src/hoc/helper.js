import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { myInfo } from '../_redux/actions/user_actions';
import { resetAllChatInfo } from '../_redux/actions/modal_action';
import SockJS from 'sockjs-client';
import * as StompJs from '@stomp/stompjs';

// option true: 헬퍼만 접근가능
// option false: 일반 유저만 접근 가능
export default function (WrappedComponent, option = true) {
  function HelperCheck() {
    const navigate = useNavigate();
    const dispatch = useDispatch();

    const isLoggedIn = useSelector((state) => state.user.isLoggedIn);
    const userInfo = useSelector((state) => state.user.userInfo);

    const [isCheckedPermission, setIsCheckedPermission] = useState(false);
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
      if (!isLoggedIn) {
        dispatch(resetAllChatInfo());
        navigate('/signIn');
        return;
      }
      if (!userInfo || isCheckedPermission) {
        return;
      }

      if (userInfo.isHelper) {
        if (!option) {
          navigate('/');
          return;
        }
      } else {
        if (option || userInfo.isAdmin) {
          navigate('/');
          return;
        }
      }
      setIsCheckedPermission(true);

      dispatch(myInfo()).catch(() => {});
    }, [isLoggedIn, userInfo]);

    useEffect(() => {
      if (!userInfo?.id || userInfo.isAdmin) {
        return;
      }
      const client = new StompJs.Client();
      client.webSocketFactory = function () {
        return new SockJS(process.env.REACT_APP_BACKEND_HOST + '/ws');
      };
      client.onConnect = () => {
        client.subscribe(`/ws/topic/${userInfo.id}/**`, () => {
          dispatch(myInfo());
        });
      };
      client.activate();
      setLoaded(true);

      return () => {
        client.deactivate();
      };
    }, [userInfo?.id]);

    return loaded && WrappedComponent;
    // return WrappedComponent;
  }

  return <HelperCheck />;
}
