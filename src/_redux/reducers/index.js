import { combineReducers } from 'redux';
import user from './user_reducer';
import modal from './modal_reducer';
import map from './map_reducer';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
  key: 'root',
  storage,
};

const rootReducer = combineReducers({ user, modal, map });
const persistedReducer = persistReducer(persistConfig, rootReducer);

export default persistedReducer;
