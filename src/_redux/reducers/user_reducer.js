import {
  LOGIN_WAIT,
  LOGIN_OK,
  MY_INFO_OK,
  MY_INFO_ERR,
  LOGOUT,
  REAUTH_OK,
  REAUTH_ERR,
} from '../actions/types';
import axios from 'axios';

export default function (state = {}, action) {
  switch (action.type) {
    case LOGIN_WAIT:
      return { ...state, isLoggedIn: false };
    case LOGIN_OK:
      localStorage.removeItem('token');
      localStorage.setItem('token', action.payload.data.token);
      axios.defaults.headers.common[
        'Authorization'
      ] = `Bearer ${action.payload.data.token}`;
      return { ...state, isLoggedIn: true };
    case REAUTH_OK:
      localStorage.removeItem('token');
      localStorage.setItem('token', action.payload.data.token);
      axios.defaults.headers.common[
        'Authorization'
      ] = `Bearer ${action.payload.data.token}`;
      return { ...state, isLoggedIn: true };
    case REAUTH_ERR:
      localStorage.removeItem('token');
      delete axios.defaults.headers.common['Authorization'];
      delete state.userInfo;
      return { ...state, isLoggedIn: false };
    case MY_INFO_OK:
      return { ...state, userInfo: action.payload };
    case MY_INFO_ERR:
      delete state.userInfo;
      return { ...state, isLoggedIn: false };
    case LOGOUT:
      localStorage.removeItem('token');
      delete axios.defaults.headers.common['Authorization'];
      delete state.userInfo;
      return { ...state, isLoggedIn: false };
    default:
      return state;
  }
}
