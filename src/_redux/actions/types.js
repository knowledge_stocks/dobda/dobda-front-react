export const WAIT = 'WAIT';
export const OK = 'OK';
export const ERR = 'ERR';

export const MY_INFO = 'MY_INFO';
export const MY_INFO_WAIT = MY_INFO.concat('_', WAIT);
export const MY_INFO_OK = MY_INFO.concat('_', OK);
export const MY_INFO_ERR = MY_INFO.concat('_', ERR);

export const LOGIN = 'LOGIN';
export const LOGIN_WAIT = LOGIN.concat('_', WAIT);
export const LOGIN_OK = LOGIN.concat('_', OK);
export const LOGIN_ERR = LOGIN.concat('_', ERR);

export const REAUTH = 'REAUTH';
export const REAUTH_WAIT = REAUTH.concat('_', WAIT);
export const REAUTH_OK = REAUTH.concat('_', OK);
export const REAUTH_ERR = REAUTH.concat('_', ERR);

export const LOGOUT = 'LOGOUT';

export const TOGGLE_CHATROOM_LIST = 'TOGGLE_CHATROOM_LIST';
export const OPEN_CHATROOM_LIST = 'OPEN_CHATROOM_LIST';
export const CLOSE_CHATROOM_LIST = 'CLOSE_CHATROOM_LIST';

export const SET_CHATROOM_INFO = 'SET_CHATROOM_INFO';

export const RESET_ALL_CHAT_INFO = 'RESET_ALL_CHAT_INFO';

export const CREATE_CHATTING_ROOM = 'CREATE_CHATTING_ROOM';
export const CREATE_CHATTING_ROOM_WAIT = CREATE_CHATTING_ROOM.concat('_', WAIT);
export const CREATE_CHATTING_ROOM_OK = CREATE_CHATTING_ROOM.concat('_', OK);
export const CREATE_CHATTING_ROOMO_ERR = CREATE_CHATTING_ROOM.concat('_', ERR);

export const MAP_CENTER = 'MAP_CENTER';
export const MAP_ZOOM = 'MAP_ZOOM';
