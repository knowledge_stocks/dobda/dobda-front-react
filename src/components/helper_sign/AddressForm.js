import React, { useEffect, useCallback, useState } from 'react';
import './AddressForm.css';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Finish from './Finish';
import DaumPostModal from '../common/DaumPostModal';
import AccountListModal from '../payment/AccountListModal';

export default function AddressForm({
  image,
  setImage,
  address,
  setAddress,
  account,
  setAccount,
}) {
  const [accountText, setAccountText] = useState('');
  const [openDaumModal, setOpenDaumModal] = useState(false);
  const [openAccountListModal, setOpenAccountListModal] = useState(false);

  useEffect(() => {
    if (!account) {
      return;
    }

    setAccountText(
      `${account.bankName} / ${account.accountNumMasked} / ${account.accountHolderName}`
    );
  }, [account]);

  const onReturnAddress = useCallback((data) => {
    setAddress?.(data.address);
  }, []);

  const onSelectAccount = useCallback((accountInfo) => {
    setAccount?.(accountInfo);
  }, []);

  return (
    <React.Fragment>
      <Typography variant='h6' gutterBottom>
        헬퍼정보 등록
      </Typography>
      <Grid container>
        <Grid item xs={12}>
          <TextField
            required
            id='helper_address'
            name='helper_address'
            label='주소'
            fullWidth
            autoComplete='shipping address-line1'
            variant='standard'
            value={address}
            onClick={() => setOpenDaumModal(true)}
            InputProps={{
              readOnly: true,
            }}
            onChange={({ target }) => setAddress(target.value)}
            InputLabelProps={{ shrink: address ? true : false }}
          />
          {openDaumModal && (
            <DaumPostModal
              onClose={() => setOpenDaumModal(false)}
              onComplete={onReturnAddress}
              closeWhenClickBack
            />
          )}
          <TextField
            sx={{ marginTop: '15px' }}
            required
            label='지급 계좌'
            fullWidth
            autoComplete='shipping address-line1'
            variant='standard'
            value={accountText}
            onClick={() => setOpenAccountListModal(true)}
            InputProps={{
              readOnly: true,
            }}
            InputLabelProps={{ shrink: account ? true : false }}
          />
          {openAccountListModal && (
            <AccountListModal
              onClose={() => setOpenAccountListModal(false)}
              onSelectAccount={onSelectAccount}
            />
          )}
        </Grid>
      </Grid>
      <Finish image={image} setImage={setImage} />
    </React.Fragment>
  );
}
