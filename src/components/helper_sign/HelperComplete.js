import React from 'react';
import Typography from '@mui/material/Typography';

export default function HelperComplete() {
  return (
    <React.Fragment>
      <Typography variant='h5' gutterBottom>
        헬퍼 신청완료
      </Typography>
      <Typography variant='subtitle1'>
        헬퍼 신청이 완료되었습니다. 첫 도움 신청을 받아보세요!
      </Typography>
    </React.Fragment>
  );
}
