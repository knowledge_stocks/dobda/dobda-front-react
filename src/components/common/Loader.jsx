import './Loader.css';

import React, { memo } from 'react';
import ReactLoading from 'react-loading';

function Loader({ size = '64px' }) {
  return (
    <div className='Loader_root'>
      <ReactLoading type='spin' color='#b2d9e0' height={size} width={size} />
    </div>
  );
}

export default memo(Loader);
