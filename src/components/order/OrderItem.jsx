import './OrderItem.css';

import React, { useCallback, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import Chip from '@mui/material/Chip';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import IconButton from '@mui/material/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMessage } from '@fortawesome/free-solid-svg-icons';
import orderStateList from '../../types/orderStateList';
import { timeForToday } from '../../util/utils';
import { createChatRoom } from '../../_redux/actions/modal_action';
import {
  approveOrder,
  cancelOrder,
  completeOrder,
  retryRefund,
  retryCalc,
} from '../../axios/axios_actions';
import AccountListModal from '../payment/AccountListModal';
import ViewComment from '../review/ViewComment';
import Comment from '../review/Comment';

function OrderItem({
  orderInfo,
  onApprove,
  onCancel,
  onComplete,
  onRetry,
  onSelectAccount,
  onConfirmReview,
  isHelperProfile = false,
}) {
  const dispatch = useDispatch();

  const userInfo = useSelector((state) => state.user.userInfo);

  const [state, setState] = useState();
  const [requesterIsMe, setRequesterIsMe] = useState(false);
  const [helperIsMe, setHelperIsMe] = useState(false);
  const [requesterReview, setRequesterReview] = useState();
  const [helperReview, setHelperReview] = useState();
  const [viewReview, setViewReview] = useState();
  const [targetReview, setTargetReview] = useState();
  const [openAccountListModal, setOpenAccountListModal] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  useEffect(() => {
    if (!orderInfo) {
      return;
    }
    setState(orderStateList.find((v) => v.code == orderInfo.state)?.label);

    const requesterId = orderInfo.requestInfo?.writerInfo?.id;
    const helperId = orderInfo.helperInfo?.id;

    setHelperIsMe(!userInfo?.isAdmin && userInfo?.id == helperId);
    setRequesterIsMe(!userInfo?.isAdmin && userInfo?.id == requesterId);
    orderInfo?.reviews?.forEach((review) => {
      if (review.writerInfo?.id == requesterId) {
        setRequesterReview(review);
      } else if (review.writerInfo?.id == helperId) {
        setHelperReview(review);
      }
    });
  }, [orderInfo]);

  const onFallbackImg = useCallback((event) => {
    event.currentTarget.onerror = null;
    event.currentTarget.src = '/user.png';
  });

  const handleClick = useCallback((event) => {
    setAnchorEl(event.currentTarget);
  }, []);

  const handleClose = useCallback(() => {
    setAnchorEl(null);
  }, []);

  const onClickMenu = useCallback((idx) => {
    alert(idx);
    setAnchorEl(null);
  }, []);

  const onClickMessage = useCallback(() => {
    if (!userInfo) {
      return alert('로그인을 해주세요');
    }

    const id = isHelperProfile
      ? orderInfo?.helperInfo?.id
      : orderInfo?.requestInfo.writerInfo?.id;

    if (id) {
      dispatch(createChatRoom(id));
    }
  }, [orderInfo, isHelperProfile]);

  const onClickReject = useCallback(() => {
    handleClose();
    if (!window.confirm('정말 거절하시겠습니까?')) {
      return;
    }

    cancelOrder(orderInfo?.id)
      .then(() => {
        alert('주문을 거절했습니다.');
        onCancel?.(orderInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [orderInfo]);

  const onClickCancel = useCallback(() => {
    handleClose();
    if (!window.confirm('정말 취소하시겠습니까?')) {
      return;
    }

    cancelOrder(orderInfo?.id)
      .then(() => {
        alert('주문을 취소했습니다.');
        onCancel?.(orderInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [orderInfo]);

  const onClickApprove = useCallback(() => {
    handleClose();
    if (
      !window.confirm(
        '정말 수락하시겠습니까?\n의뢰를 수락한 뒤에는 취소가 불가능 합니다.'
      )
    ) {
      return;
    }

    approveOrder(orderInfo?.id)
      .then(() => {
        alert('주문을 수락했습니다.');
        onApprove?.(orderInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [orderInfo]);

  const onClickComplete = useCallback(() => {
    handleClose();
    if (!window.confirm('정말 완료하시겠습니까?')) {
      return;
    }

    completeOrder(orderInfo?.id)
      .then(() => {
        alert('주문을 완료했습니다.');
        onComplete?.(orderInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [orderInfo]);

  const onClickAccount = useCallback(() => {
    setOpenAccountListModal(true);
  }, [orderInfo]);

  const onClickRetryCalc = useCallback(() => {
    if (!window.confirm('계정에 설정된 계좌로 정산을 시도합니다.')) {
      return;
    }

    retryCalc(orderInfo?.id)
      .then(() => {
        alert('정산을 성공했습니다.');
        onRetry?.(orderInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [orderInfo]);

  const onClickRetryRefund = useCallback(() => {
    if (!window.confirm('거래에 설정된 계좌로 환불을 시도합니다.')) {
      return;
    }

    retryRefund(orderInfo?.id)
      .then(() => {
        alert('환불을 성공했습니다.');
        onRetry?.(orderInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [orderInfo]);

  const onSelectAccountInternal = useCallback(
    (accountInfo) => {
      onSelectAccount?.(accountInfo);
      handleClose();
    },
    [onSelectAccount]
  );

  const onConfirmReviewInternal = useCallback(
    (reviewInfo) => {
      onConfirmReview?.(reviewInfo);
      handleClose();
    },
    [onConfirmReview]
  );

  return (
    <div className='OrderItem_root'>
      <Link
        to={`/view?id=${orderInfo?.requestInfo.requestId}`}
        className='OrderItem_title-box'
      >
        <span className='OrderItem_state'>{state}</span>
        <span className='OrderItem_title'>{orderInfo?.requestInfo.title}</span>
      </Link>
      <div className='OrderItem_profile'>
        <div className='OrderItem_profile-image-box'>
          <div className='OrderItem_rounding-profile-image'>
            <img src='' onError={onFallbackImg} />
          </div>
        </div>
        <div className='OrderItem_profile-detail-box'>
          <div className='OrderItem_profile-name-box'>
            <span className='OrderItem_profile-name'>
              {isHelperProfile
                ? orderInfo?.helperInfo?.nickname ?? '탈퇴한 사용자'
                : orderInfo?.requestInfo.writerInfo?.nickname ??
                  '탈퇴한 사용자'}
            </span>
            {!userInfo?.isAdmin &&
              (isHelperProfile ? !helperIsMe : !requesterIsMe) && (
                <FontAwesomeIcon
                  icon={faMessage}
                  className='RequestItem_message-icon'
                  onClick={onClickMessage}
                />
              )}
          </div>
          <div className='OrderItem_address-box'>
            <span className='OrderItem_address'>
              {isHelperProfile
                ? orderInfo?.helperInfo?.helperInfo.address
                : orderInfo?.requestInfo.address1}
            </span>
            <LocationOnIcon className='OrderItem_location-icon' />
          </div>
        </div>
      </div>
      <div className='OrderItem_condition'>
        <div className='OrderItem_condition-left'>
          <Chip
            className='OrderItem_payment'
            label={'₩ ' + orderInfo?.amount.toLocaleString('ko-KR')}
            color='primary'
            variant='outlined'
          />
          {orderInfo?.payMethod == 2 ? (
            <Chip
              className='OrderItem_payment-account'
              label='계좌 이체'
              color='primary'
              variant='outlined'
            />
          ) : (
            orderInfo?.payMethod == 1 && (
              <Chip
                className='OrderItem_payment-cash'
                label='현금 직접 결제'
                color='primary'
              />
            )
          )}
        </div>
        {((helperIsMe && orderInfo?.state != 10 && orderInfo?.state != 60) ||
          requesterIsMe) &&
          orderInfo?.state != 50 && (
            <div className='OrderItem_condition-right'>
              <IconButton
                aria-label='more'
                className='OrderItem_long-button'
                aria-controls={open ? 'long-menu' : undefined}
                aria-expanded={open ? 'true' : undefined}
                aria-haspopup='ture'
                onClick={handleClick}
              >
                <MoreVertIcon />
              </IconButton>
              <Menu
                MenuListProps={{ 'aria-labelledby': 'long-button' }}
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
              >
                {helperIsMe ? (
                  <div>
                    {orderInfo?.state == 0 ? (
                      <div>
                        <MenuItem onClick={onClickApprove}>수락하기</MenuItem>
                        <MenuItem onClick={onClickReject}>거절하기</MenuItem>
                      </div>
                    ) : orderInfo?.state == 40 || orderInfo?.state == 70 ? (
                      <div>
                        {orderInfo?.state == 70 && (
                          <div>
                            <MenuItem onClick={onClickRetryCalc}>
                              정산 재시도
                            </MenuItem>
                            <MenuItem onClick={onClickAccount}>
                              헬퍼 입금계좌 변경
                            </MenuItem>
                          </div>
                        )}
                        {requesterReview && (
                          <MenuItem
                            onClick={() => setViewReview(requesterReview)}
                          >
                            받은 후기 보기
                          </MenuItem>
                        )}
                        {helperReview ? (
                          <MenuItem onClick={() => setViewReview(helperReview)}>
                            보낸 후기 보기
                          </MenuItem>
                        ) : (
                          <MenuItem
                            onClick={() =>
                              setTargetReview(
                                orderInfo?.requestInfo?.writerInfo
                              )
                            }
                          >
                            후기 보내기
                          </MenuItem>
                        )}
                      </div>
                    ) : null}
                  </div>
                ) : (
                  <div>
                    {orderInfo?.state == 10 ? (
                      <MenuItem onClick={onClickComplete}>완료하기</MenuItem>
                    ) : orderInfo?.state == 0 ? (
                      <MenuItem onClick={onClickCancel}>취소하기</MenuItem>
                    ) : orderInfo?.state == 60 ? (
                      <div>
                        <MenuItem onClick={onClickRetryRefund}>
                          환불 재시도
                        </MenuItem>
                        <MenuItem onClick={onClickAccount}>
                          입금계좌 일괄 변경
                        </MenuItem>
                      </div>
                    ) : (
                      (orderInfo?.state == 40 || orderInfo?.state == 70) && (
                        <div>
                          {helperReview && (
                            <MenuItem
                              onClick={() => setViewReview(helperReview)}
                            >
                              받은 후기 보기
                            </MenuItem>
                          )}
                          {requesterReview ? (
                            <MenuItem
                              onClick={() => setViewReview(requesterReview)}
                            >
                              보낸 후기 보기
                            </MenuItem>
                          ) : (
                            <MenuItem
                              onClick={() =>
                                setTargetReview(orderInfo?.helperInfo)
                              }
                            >
                              후기 보내기
                            </MenuItem>
                          )}
                        </div>
                      )
                    )}
                  </div>
                )}
                {openAccountListModal && (
                  <AccountListModal
                    onClose={() => setOpenAccountListModal(false)}
                    onSelectAccount={onSelectAccountInternal}
                  />
                )}
              </Menu>
              <ViewComment
                open={viewReview != null}
                reviewInfo={viewReview}
                handleClose={() => setViewReview()}
              />
              <Comment
                open={targetReview != null}
                handleClose={() => setTargetReview()}
                targetInfo={targetReview}
                orderId={orderInfo?.id}
                onConfrim={onConfirmReviewInternal}
              />
            </div>
          )}
      </div>
      <div className='OrderItem_view-count-box'>
        <time className='reg_time'>{timeForToday(orderInfo?.regTime)}</time>
      </div>
    </div>
  );
}

export default OrderItem;
