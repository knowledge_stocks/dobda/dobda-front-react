import './NotiItem.css';

import React, { useCallback, useState } from 'react';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import IconButton from '@mui/material/IconButton';
import { timeForToday } from '../../util/utils';
import { removeNoti } from '../../axios/axios_actions';

function NotiItem({ notiInfo, onDelete }) {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  const onFallbackImg = useCallback((event) => {
    event.currentTarget.onerror = null;
    event.currentTarget.src = '/user.png';
  });

  const handleClick = useCallback((event) => {
    setAnchorEl(event.currentTarget);
  }, []);

  const handleClose = useCallback(() => {
    setAnchorEl(null);
  }, []);

  const onClickMenu = useCallback((idx) => {
    alert(idx);
    setAnchorEl(null);
  }, []);

  const onClickDelete = useCallback(() => {
    handleClose();

    removeNoti(notiInfo?.id)
      .then(() => {
        onDelete?.(notiInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [notiInfo]);

  return (
    <div className='NotiItem_root'>
      <div className='NotiItem_view-count-box'>
        <div className='NotiItem_content-box'>
          <div className='NotiItem_article-detail'>{notiInfo?.message}</div>
          <div className='NotiItem_condition-right'>
            <IconButton
              aria-label='more'
              className='NotiItem_long-button'
              aria-controls={open ? 'long-menu' : undefined}
              aria-expanded={open ? 'true' : undefined}
              aria-haspopup='ture'
              onClick={handleClick}
            >
              <MoreVertIcon />
            </IconButton>
            <Menu
              MenuListProps={{ 'aria-labelledby': 'long-button' }}
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
            >
              <MenuItem onClick={onClickDelete}>삭제하기</MenuItem>
            </Menu>
          </div>
        </div>
        <time>{notiInfo?.regTime && timeForToday(notiInfo?.regTime)}</time>
      </div>
    </div>
  );
}

export default NotiItem;
