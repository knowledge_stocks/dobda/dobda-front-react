import { Box } from '@mui/system';
import React, { useCallback } from 'react';
import './HelperApproval.css';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import ClearIcon from '@mui/icons-material/Clear';
import moment from 'moment-timezone';
import {
  rejectRegisterHelper,
  approveRegisterHelper,
} from '../../axios/axios_actions';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 500,
  bgcolor: 'white',
  boxShadow: 24,
  p: 4,
};

export default function HelperApproval({ applyInfo, onReject, onApprove }) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  const onClickReject = useCallback(() => {
    if (!window.confirm('선택된 헬퍼 신청을 거부하시겠습니까?')) {
      return;
    }

    rejectRegisterHelper(applyInfo?.id)
      .then(() => {
        alert('헬퍼 신청이 거부되었습니다.');
        onReject?.(applyInfo);
        handleClose();
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [applyInfo]);

  const onClickApprove = useCallback(() => {
    if (!window.confirm('선택된 헬퍼 신청을 승인하시겠습니까?')) {
      return;
    }

    approveRegisterHelper(applyInfo?.id)
      .then(() => {
        alert('헬퍼 신청이 승인되었습니다.');
        onApprove?.(applyInfo);
        handleClose();
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [applyInfo]);

  return (
    <Box>
      <div className='Approval_wrap'>
        <div className='Approval_userID'>{applyInfo?.bankName}</div>
        <div className='Approval_address'>{applyInfo?.address}</div>
        <div className='Approval_date'>
          {moment(applyInfo?.regTime).format('YYYY.MM.DD')}
        </div>

        <Button
          className='Approval_btn'
          variant='contained'
          onClick={handleOpen}
        >
          확인
        </Button>
        <Modal open={open} onClose={handleClose}>
          <Box sx={style} className='identity_wrap'>
            <ClearIcon className='Approval_exit' onClick={handleClose} />
            <Typography className='identity_text'>주민등록증</Typography>
            <img
              className='Approval_img'
              src={process.env.REACT_APP_BACKEND_HOST + applyInfo?.idCard}
              width='400'
              height='200'
            ></img>

            <Button
              className='ApprovalLast_reject-btn'
              variant='contained'
              onClick={onClickReject}
            >
              거부
            </Button>
            <Button
              className='ApprovalLast_btn'
              variant='contained'
              onClick={onClickApprove}
            >
              승인
            </Button>
          </Box>
        </Modal>
      </div>
      {/* <div className='Approval_Cwrap'>
        <div className='Approval_CuserID'>user2</div>
        <div className='Approval_Caddress'>서울 영등포구 OO대로34길 16 </div>
        <div className='Approval_Cdate'>2022.03.14</div>

        <Button
          className='Approval_Cbtn'
          variant='contained'
          sx={{ color: 'gray !important' }}
          disabled
        >
          완료
        </Button>
      </div> */}
    </Box>
  );
}
