import './ChatRoomList.css';

import React, { useEffect, useState } from 'react';
import ChatRoomItem from './ChatRoomItem';
import Loader from '../common/Loader';
import { useInView } from 'react-intersection-observer';
import { useSelector } from 'react-redux';
import SockJS from 'sockjs-client';
import * as StompJs from '@stomp/stompjs';
import { getChatRoomList } from '../../axios/axios_actions';

function ChatRoomList() {
  const userInfo = useSelector((state) => state.user.userInfo);

  const [chatRoomList, setChatRoomList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [isEnd, setIsEnd] = useState(false);
  const [bottomRef, inView] = useInView();

  useEffect(() => {
    const client = new StompJs.Client();
    client.webSocketFactory = function () {
      return new SockJS(process.env.REACT_APP_BACKEND_HOST + '/ws');
    };
    client.onConnect = () => {
      client.subscribe(`/ws/topic/${userInfo.id}/chat/*`, (msg) => {
        const parsedMsg = JSON.parse(msg.body);
        setChatRoomList((chatRoomList) =>
          [...chatRoomList].map((v) => {
            if (v.id == parsedMsg.roomId) {
              v.hasUnreadMsg = parsedMsg.senderId != userInfo.id;
              v.lastMessage = parsedMsg.message;
              v.lastTime = parsedMsg.regTime;
            }
            return v;
          })
        );
      });
      client.subscribe(`/ws/topic/${userInfo.id}/readchat/*`, (msg) => {
        setChatRoomList((chatRoomList) =>
          [...chatRoomList].map((v) => {
            if (v.id == msg.body) {
              v.hasUnreadMsg = false;
            }
            return v;
          })
        );
      });
    };
    client.activate();

    return () => {
      client.deactivate();
    };
  }, []);

  useEffect(async () => {
    if (page === 0) {
      return;
    }
    setIsLoading(true);
    try {
      const response = await getChatRoomList(page);
      setChatRoomList((chatRoomList) => [
        ...chatRoomList,
        ...response.data.content,
      ]);
      setIsEnd(response.data.last);
    } catch {
      setPage((prevState) => prevState - 1);
    }
    await new Promise((resolve) => setTimeout(resolve, 500));
    setIsLoading(false);
  }, [page]);

  useEffect(() => {
    if (inView && !isLoading && !isEnd) {
      setPage((prevState) => prevState + 1);
    }
  }, [inView, isLoading, isEnd]);

  return (
    <main className='ChatRoomList_root'>
      <div className='ChatRoomList_list'>
        {chatRoomList.map((v, idx) => (
          <ChatRoomItem chatRoomInfo={v} key={idx} />
        ))}
        <div ref={bottomRef} className='ChatRoomList_bottom'>
          {isLoading && <Loader size='32px' />}
        </div>
      </div>
    </main>
  );
}

export default ChatRoomList;
