import './ChatItem.css';

import React from 'react';
import { useSelector } from 'react-redux';
import moment from 'moment';

function ChatItem({ chatInfo }) {
  const userInfo = useSelector((state) => state.user.userInfo);

  return (
    <div className='ChatItem_root'>
      <div
        className={
          'ChatItem_desc' +
          (chatInfo?.senderId == userInfo.id ? ' ChatItem_me' : '')
        }
      >
        <span className='ChatItem_test'>{chatInfo?.message}</span>
        <span>{moment(chatInfo?.regTime ?? 0).format('YYYY/MM/DD HH:mm')}</span>
      </div>
    </div>
  );
}

export default ChatItem;
