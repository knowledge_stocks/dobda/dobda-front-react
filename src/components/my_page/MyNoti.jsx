import './MyNoti.css';
import React, { useEffect, useState, useCallback } from 'react';
import NotiItem from '../noti/NotiItem';
import Loader from '../common/Loader';
import { useInView } from 'react-intersection-observer';
import { useSelector } from 'react-redux';
import SockJS from 'sockjs-client';
import * as StompJs from '@stomp/stompjs';
import { readNoti, getNotiList } from '../../axios/axios_actions';

function MyNoti() {
  const userInfo = useSelector((state) => state.user.userInfo);

  const [notiList, setNotiList] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [lastId, setLastId] = useState();
  const [isEnd, setIsEnd] = useState(false);
  const [bottomRef, inView] = useInView();

  useEffect(() => {
    const client = new StompJs.Client();
    client.webSocketFactory = function () {
      return new SockJS(process.env.REACT_APP_BACKEND_HOST + '/ws');
    };
    client.onConnect = () => {
      client.subscribe(`/ws/topic/${userInfo.id}/noti`, (msg) => {
        const parsedMsg = JSON.parse(msg.body);
        setNotiList((notiList) => [parsedMsg, ...notiList]);
        readNoti();
      });
    };
    client.activate();

    return () => {
      client.deactivate();
    };
  }, []);

  useEffect(async () => {
    try {
      setIsLoading(true);
      const response = await getNotiList(lastId);
      setNotiList((notiList) => [...notiList, ...response.data.content]);
      setIsEnd(response.data.last);
    } catch {}
    await new Promise((resolve) => setTimeout(resolve, 500));
    setIsLoading(false);
  }, [lastId]);

  useEffect(() => {
    if (inView && !isLoading && !isEnd) {
      setLastId(notiList[notiList.length - 1]?.id);
    }
  }, [inView, isLoading, isEnd, notiList]);

  const onDelete = useCallback((notiInfo) => {
    if (!notiInfo) {
      return;
    }
    setNotiList((notiList) => notiList.filter((v) => v.id != notiInfo.id));
  }, []);

  return (
    <div className='MyNoti_root'>
      <div className='MyNoti_list'>
        {notiList.map((v, idx) => (
          <NotiItem notiInfo={v} key={idx} onDelete={onDelete} />
        ))}
        <div ref={bottomRef} className='MyNoti_bottom'>
          {isLoading && <Loader />}
        </div>
      </div>
    </div>
  );
}

export default MyNoti;
