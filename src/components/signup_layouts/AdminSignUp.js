import './AdminSignUp.css';
import React, { useCallback, useState } from 'react';
import { useNavigate, Link } from 'react-router-dom';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Card from '@mui/material/Card';
import {
  adminEmailCheck,
  adminNicknameCheck,
  addAdmin,
  adminPhoneNumCheck,
} from '../../axios/axios_actions';

// 이메일 패턴
const emailPattern = /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
// pwd는 8~30자로 영문, 숫자, 특수문자를 한번씩은 반드시 사용해야 한다.
const pwdPattern =
  /^(?=.*[a-zA-Z])(?=.*[\d])(?=.*[!@#$%^&_*])[a-zA-Z\d!@#$%^&_*]{8,30}$/;
// 이름은 3~10자로 일반문자와 숫자로만 구성되고 반드시 첫 문자가 문자이어야 한다.
const nicknamePattern = /^\p{L}[\p{L}\d]{2,19}$/u;

function Copyright(props) {
  return (
    <Typography
      variant='body2'
      color='text.secondary'
      align='center'
      {...props}
    >
      {'Copyright © '}
      <Link color='inherit' to='/'>
        DOBDA
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

export default function AdminSignUp() {
  const navigate = useNavigate();

  const [email, setEmail] = useState('');
  const [isValidEmail, setIsValidEmail] = useState(true);
  const [isUniqueEmail, setIsUniqueEmail] = useState(false);

  const [pwd, setPwd] = useState('');
  const [isValidPwd, setIsValidPwd] = useState(true);

  const [name, setName] = useState('');
  const [isValidName, setIsValidName] = useState(true);

  const [nickname, setNickname] = useState('');
  const [isValidNickname, setIsValidNickname] = useState(true);
  const [isUniqueNickname, setIsUniqueNickname] = useState(false);

  const [phoneNum, setPhoneNum] = useState('');
  const [isUniquePhoneNum, setIsUniquePhoneNum] = useState(false);

  const onChangeEmail = useCallback((event) => {
    const email = event.target.value;
    setEmail(email);
    setIsValidEmail(emailPattern.test(email));
    setIsUniqueEmail(false);
  });

  const onChangePwd = useCallback((event) => {
    const pwd = event.target.value;
    setPwd(pwd);
    setIsValidPwd(pwdPattern.test(pwd));
  });

  const onChangeName = useCallback((event) => {
    const name = event.target.value;
    setName(name);
    setIsValidName(name.length > 1);
  });

  const onChangeNickname = useCallback((event) => {
    const nickname = event.target.value;
    setNickname(nickname);
    setIsValidNickname(nicknamePattern.test(nickname));
    setIsUniqueNickname(false);
  });

  const onChangePhoneNum = useCallback((event) => {
    const phoneNum = event.target.value;
    setPhoneNum(phoneNum);
    setIsUniquePhoneNum(false);
  });

  const onClickEmailCheck = useCallback(
    (e) => {
      e.preventDefault();
      if (email == '' || !isValidEmail) {
        return alert('이메일을 올바르게 입력해주세요.');
      }

      adminEmailCheck(email)
        .then((response) => {
          if (response.data) {
            alert('사용 가능한 아이디 입니다.😊');
            setIsUniqueEmail(true);
          } else {
            alert('중복된 아이디 입니다.😥');
          }
        })
        .catch((err) => {
          if (err.response) {
            alert(err.response.data);
          } else if (err.request) {
            alert('서버가 응답하지 않습니다.');
          } else {
            alert('잘못된 요청입니다.');
          }
        });
    },
    [email]
  );

  const onClickNicknameCheck = useCallback(
    (e) => {
      e.preventDefault();
      if (nickname == '' || !isValidNickname) {
        return alert('닉네임을 올바르게 입력해주세요.');
      }

      adminNicknameCheck(nickname)
        .then((response) => {
          if (response.data) {
            alert('사용 가능한 닉네임 입니다.😊');
            setIsUniqueNickname(true);
          } else {
            alert('중복된 닉네임 입니다.😥');
          }
        })
        .catch((err) => {
          if (err.response) {
            alert(err.response.data);
          } else if (err.request) {
            alert('서버가 응답하지 않습니다.');
          } else {
            alert('잘못된 요청입니다.');
          }
        });
    },
    [nickname]
  );

  const onClickPhoneNumCheck = useCallback(
    (e) => {
      e.preventDefault();
      if (phoneNum == '') {
        return alert('전화번호를 올바르게 입력해주세요.');
      }

      adminPhoneNumCheck(phoneNum)
        .then((response) => {
          if (response.data) {
            alert('사용 가능한 전화번호 입니다.😊');
            setIsUniquePhoneNum(true);
          } else {
            alert('중복된 전화번호 입니다.😥');
          }
        })
        .catch((err) => {
          if (err.response) {
            alert(err.response.data);
          } else if (err.request) {
            alert('서버가 응답하지 않습니다.');
          } else {
            alert('잘못된 요청입니다.');
          }
        });
    },
    [phoneNum]
  );

  const onClickSignUp = useCallback(
    (e) => {
      e.preventDefault();
      if (!isUniqueEmail) {
        return alert('이메일 중복 확인을 해주세요.😥');
      }
      if (!isUniqueNickname) {
        return alert('닉네임 중복 확인을 해주세요.😥');
      }
      if (!isUniquePhoneNum) {
        return alert('전화번호 중복 확인을 해주세요.😥');
      }
      if (name == '' || !isValidName || pwd == '' || !isValidPwd) {
        // if (name == '' || !isValidName || pwd == '') {
        return alert('입력된 내용이 올바르지 않습니다.😥');
      }

      addAdmin(email, pwd, name, nickname, phoneNum)
        .then(() => {
          alert('관리자 등록을 성공했습니다.😊');
          navigate('/admin/signIn');
        })
        .catch((err) => {
          if (err.response) {
            alert(err.response.data);
          } else if (err.request) {
            alert('서버가 응답하지 않습니다.');
          } else {
            alert('잘못된 요청입니다.');
          }
        });
    },
    [
      email,
      pwd,
      name,
      nickname,
      phoneNum,
      isUniqueEmail,
      isUniqueNickname,
      isValidName,
      isValidPwd,
      isUniquePhoneNum,
    ]
  );

  return (
    <Card className='AdminSignUp_root' sx={{ maxWidth: 500 }}>
      <Container
        className='AdminSignUp_container'
        component='main'
        maxWidth='xs'
      >
        <Box id='box'>
          <Typography
            id='BrandName'
            sx={{ mt: 6, mb: 3 }}
            component='h1'
            variant='h5'
          >
            DOBDA
          </Typography>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={8}>
              <TextField
                className={isValidEmail ? '' : 'AdminSignUp_invalidate'}
                margin='normal'
                label='이메일'
                value={email}
                onChange={onChangeEmail}
                required
                fullWidth
                autoComplete='email'
                autoFocus
                sx={{ mb: 2 }}
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <Button
                className='AdminSignUp_button'
                margin='normal'
                fullWidth
                required
                variant='contained'
                onClick={onClickEmailCheck}
                disabled={isUniqueEmail}
              >
                {isUniqueEmail ? '확인 완료' : '중복 확인'}
              </Button>
            </Grid>
          </Grid>
          <TextField
            className={isValidPwd ? '' : 'AdminSignUp_invalidate'}
            label='비밀번호'
            type='password'
            required
            fullWidth
            value={pwd}
            onChange={onChangePwd}
            autoComplete='current-password'
            sx={{ mb: 1 }}
          />
          <p>
            * 8~30자 이내 영문,숫자,특수문자를 한번씩 조합해야 합니다.
            <br />* 사용 가능한 특수문자: !@#$%^&amp;_*
          </p>

          <TextField
            className={isValidName ? '' : 'AdminSignUp_invalidate'}
            label='이름'
            margin='normal'
            required
            fullWidth
            value={name}
            onChange={onChangeName}
            autoComplete='given-name'
          />

          <Grid container spacing={2}>
            <Grid item xs={12} sm={8}>
              <TextField
                className={isValidNickname ? '' : 'AdminSignUp_invalidate'}
                label='닉네임'
                id='nickname'
                margin='normal'
                required
                fullWidth
                value={nickname}
                onChange={onChangeNickname}
                autoComplete='nickname'
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <Button
                className='AdminSignUp_button'
                margin='normal'
                fullWidth
                required
                variant='contained'
                onClick={onClickNicknameCheck}
                disabled={isUniqueNickname}
              >
                {isUniqueNickname ? '확인 완료' : '중복 확인'}
              </Button>
            </Grid>
          </Grid>
          <p>
            * 닉네임은 3~20자 이내 일반문자, 숫자로만 구성되어야 합니다.
            <br />* 첫 문자는 일반문자만 가능합니다.
          </p>

          <Grid container spacing={2}>
            <Grid item xs={12} sm={8}>
              <TextField
                label='전화번호 입력'
                id='phone_num'
                margin='normal'
                required
                fullWidth
                value={phoneNum}
                onChange={onChangePhoneNum}
                autoComplete='phone_num'
              />
            </Grid>

            <Grid item xs={12} sm={4}>
              <Button
                className='AdminSignUp_button'
                margin='normal'
                fullWidth
                required
                variant='contained'
                onClick={onClickPhoneNumCheck}
                disabled={isUniquePhoneNum}
              >
                {isUniquePhoneNum ? '확인 완료' : '중복 확인'}
              </Button>
            </Grid>
          </Grid>

          <Button
            id='login_btn'
            type='submit'
            fullWidth
            variant='contained'
            sx={{ mt: 4, mb: 3 }}
            onClick={onClickSignUp}
          >
            회원가입
          </Button>
          <Grid container id='AdminSignUp_btn'>
            <Grid item xs>
              <Link to='/admin/signIn'>로그인</Link>
            </Grid>
          </Grid>
        </Box>
        <Copyright sx={{ mt: 2, mb: 3 }} />
      </Container>
    </Card>
  );
}
