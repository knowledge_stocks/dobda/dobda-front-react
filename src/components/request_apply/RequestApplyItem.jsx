import './RequestApplyItem.css';

import React, { useCallback, useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import Chip from '@mui/material/Chip';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import IconButton from '@mui/material/IconButton';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMessage } from '@fortawesome/free-solid-svg-icons';
import requestApplyStateList from '../../types/requestApplyStateList';
import { timeForToday } from '../../util/utils';
import { createChatRoom } from '../../_redux/actions/modal_action';
import {
  deleteRequestApply,
  rejectRequestApply,
} from '../../axios/axios_actions';
import View_OfferForm from '../write_layouts/View_OfferForm';
import PaymentModal from '../payment/PaymentModal';

function RequestApplyItem({
  applyInfo,
  onReject,
  onDelete,
  onConfirm,
  onApprove,
  isHelperProfile = false,
}) {
  const dispatch = useDispatch();

  const userInfo = useSelector((state) => state.user.userInfo);

  const [state, setState] = useState();
  const [requesterIsMe, setRequesterIsMe] = useState(false);
  const [helperIsMe, setHelperIsMe] = useState(false);
  const [openOfferModal, setOpenOfferModal] = useState(false);
  const [openPaymentModal, setPaymentModal] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);

  useEffect(() => {
    if (!applyInfo) {
      return;
    }
    setState(
      requestApplyStateList.find((v) => v.code == applyInfo.state)?.label
    );
    setHelperIsMe(
      !userInfo?.isAdmin && userInfo?.id == applyInfo.helperInfo?.id
    );
    setRequesterIsMe(
      !userInfo?.isAdmin && userInfo?.id == applyInfo.requestInfo.writerInfo?.id
    );
  }, [applyInfo]);

  const onFallbackImg = useCallback((event) => {
    event.currentTarget.onerror = null;
    event.currentTarget.src = '/user.png';
  });

  const handleClick = useCallback((event) => {
    setAnchorEl(event.currentTarget);
  }, []);

  const handleClose = useCallback(() => {
    setAnchorEl(null);
  }, []);

  const onClickMessage = useCallback(() => {
    if (!userInfo) {
      return alert('로그인을 해주세요');
    }

    const id = isHelperProfile
      ? applyInfo?.helperInfo?.id
      : applyInfo?.requestInfo.writerInfo?.id;

    if (id) {
      dispatch(createChatRoom(id));
    }
  }, [applyInfo, isHelperProfile]);

  const onClickDelete = useCallback(() => {
    handleClose();
    if (!window.confirm('정말 삭제하시겠습니까?')) {
      return;
    }

    deleteRequestApply(applyInfo?.id)
      .then(() => {
        alert('제안을 삭제했습니다.');
        onDelete?.(applyInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [applyInfo]);

  const onClickApprove = useCallback(() => {
    setPaymentModal(true);
  }, [applyInfo]);

  const onApproveInternal = useCallback(() => {
    handleClose();
    if (!applyInfo) {
      return;
    }
    applyInfo.state = 30;
    onApprove?.(applyInfo);
  }, [applyInfo, onApprove]);

  const onClickReject = useCallback(() => {
    handleClose();
    if (!window.confirm('정말 거절하시겠습니까?')) {
      return;
    }

    rejectRequestApply(applyInfo?.id)
      .then(() => {
        alert('제안을 거절했습니다.');
        onReject?.(applyInfo);
      })
      .catch((err) => {
        if (err.response) {
          alert(err.response.data);
        } else if (err.request) {
          alert('서버가 응답하지 않습니다.');
        } else {
          alert('잘못된 요청입니다.');
        }
      });
  }, [applyInfo]);

  return (
    <div className='RequestApplyItem_root'>
      <Link
        to={`/view?id=${applyInfo?.requestInfo.requestId}`}
        className='RequestApplyItem_title-box'
      >
        <span className='RequestApplyItem_state'>{state}</span>
        <span className='RequestApplyItem_title'>
          {applyInfo?.requestInfo.title}
        </span>
      </Link>
      <div className='RequestApplyItem_profile'>
        <div className='RequestApplyItem_profile-image-box'>
          <div className='RequestApplyItem_rounding-profile-image'>
            <img src='' onError={onFallbackImg} />
          </div>
        </div>
        <div className='RequestApplyItem_profile-detail-box'>
          <div className='RequestApplyItem_profile-name-box'>
            <span className='RequestApplyItem_profile-name'>
              {isHelperProfile
                ? applyInfo?.helperInfo?.nickname ?? '탈퇴한 사용자'
                : applyInfo?.requestInfo.writerInfo?.nickname ??
                  '탈퇴한 사용자'}
            </span>
            {!userInfo?.isAdmin &&
              (isHelperProfile ? !helperIsMe : !requesterIsMe) && (
                <FontAwesomeIcon
                  icon={faMessage}
                  className='RequestItem_message-icon'
                  onClick={onClickMessage}
                />
              )}
          </div>
          <div className='RequestApplyItem_address-box'>
            <span className='RequestApplyItem_address'>
              {isHelperProfile
                ? applyInfo?.helperInfo?.helperInfo.address
                : applyInfo?.requestInfo.address1}
            </span>
            <LocationOnIcon className='RequestApplyItem_location-icon' />
          </div>
        </div>
      </div>
      <div className='RequestApplyItem_condition'>
        <div className='RequestApplyItem_condition-left'>
          <Chip
            className='RequestApplyItem_payment'
            label={'₩ ' + applyInfo?.amount.toLocaleString('ko-KR')}
            color='primary'
            variant='outlined'
          />
        </div>
        {(helperIsMe || requesterIsMe) && (
          <div className='RequestApplyItem_condition-right'>
            <IconButton
              aria-label='more'
              className='RequestApplyItem_long-button'
              aria-controls={open ? 'long-menu' : undefined}
              aria-expanded={open ? 'true' : undefined}
              aria-haspopup='ture'
              onClick={handleClick}
            >
              <MoreVertIcon />
            </IconButton>
            <Menu
              MenuListProps={{ 'aria-labelledby': 'long-button' }}
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
            >
              {helperIsMe ? (
                <div>
                  <MenuItem onClick={() => setOpenOfferModal(true)}>
                    수정하기
                  </MenuItem>
                  <View_OfferForm
                    open={openOfferModal}
                    onClose={() => setOpenOfferModal(false)}
                    applyId={applyInfo?.id}
                    requestId={applyInfo?.requestInfo.requestId}
                    onConfirm={onConfirm}
                  />
                  <MenuItem onClick={onClickDelete}>삭제하기</MenuItem>
                </div>
              ) : (
                <div>
                  <MenuItem onClick={onClickApprove}>수락하기</MenuItem>
                  {openPaymentModal && (
                    <PaymentModal
                      onClose={() => setPaymentModal(false)}
                      onConfirm={onApproveInternal}
                      applyId={applyInfo?.id}
                    />
                  )}
                  <MenuItem onClick={onClickReject}>거절하기</MenuItem>
                </div>
              )}
            </Menu>
          </div>
        )}
      </div>
      <div className='RequestApplyItem_view-count-box'>
        <div className='RequestApplyItem_article-detail'>
          {applyInfo?.description}
        </div>
        <time className='reg_time'>
          {applyInfo?.modTime
            ? `${timeForToday(applyInfo?.modTime)}`
            : timeForToday(applyInfo?.regTime)}
        </time>
      </div>
    </div>
  );
}

export default RequestApplyItem;
