FROM node:16.13.1
LABEL maintainer="Jaeho Lee <jhlee21071@gmail.com>"

WORKDIR /usr/src/app
RUN npm install -g serve@13.0.2
COPY .ssl/privkey.pem /usr/src/app/key.pem
COPY .ssl/cert.pem /usr/src/app/cert.pem
COPY build /usr/src/app/build

ENTRYPOINT ["serve", "-l", "80", "-s", "build", "--ssl-cert", "/usr/src/app/cert.pem", "--ssl-key", "/usr/src/app/key.pem"]